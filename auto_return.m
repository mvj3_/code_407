UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(10, 100, 300, 180)];
UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, 100, 300, 150)];
label.text = @"Hello world! Hello world!Hello world! Hello world! Hello world! Hello world! Hello world! Hello world!Hello world! Hello world! Hello world! Hello world! Hello world! Helloworld!";
//背景颜色为红色
label.backgroundColor = [UIColor redColor];
//设置字体颜色为白色
label.textColor = [UIColor whiteColor];
//文字居中显示
label.textAlignment = UITextAlignmentCenter;
//自动折行设置
label.lineBreakMode = UILineBreakModeWordWrap;
label.numberOfLines = 0;